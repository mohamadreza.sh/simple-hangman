package com.example.hangman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FinishActivity extends AppCompatActivity {
    ImageView imageView;
    Button btnRestart, btnExit;
    TextView txtStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_finish);

        imageView = findViewById(R.id.imgFinishstate);
        txtStatus = findViewById(R.id.txtStatus);
        btnRestart = findViewById(R.id.btnRestart);
        btnExit = findViewById(R.id.btnExit);

        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FinishActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        int imageid = R.drawable.f10;
        String status = "You Won";

        String result = getIntent().getStringExtra("RESULT");

        if (!result.equals("WON")) {
            imageid = R.drawable.f9;
            status = "Game over";
        }

        imageView.setImageResource(imageid);
        txtStatus.setText(status);
    }

}
