package com.example.hangman;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ImageView imgFace;
    TextView txtWord;
    private Button btnA;
    private Button btnB;
    private Button btnC;
    private Button btnD;
    private Button btnE;
    private Button btnF;
    private Button btnG;
    private Button btnH;
    private Button btnI;
    private Button btnJ;
    private Button btnK;
    private Button btnL;
    private Button btnM;
    private Button btnN;
    private Button btnO;
    private Button btnP;
    private Button btnQ;
    private Button btnR;
    private Button btnS;
    private Button btnT;
    private Button btnU;
    private Button btnV;
    private Button btnW;
    private Button btnX;
    private Button btnY;
    private Button btnZ;
    private int faildcount;

    private String selecetedWord;
    private String dash;

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtWord = findViewById(R.id.txtWord);
        imgFace = findViewById(R.id.imgFace);
        btnA = findViewById(R.id.btnA);
        btnB = findViewById(R.id.btnB);
        btnC = findViewById(R.id.btnC);
        btnD = findViewById(R.id.btnD);
        btnE = findViewById(R.id.btnE);
        btnF = findViewById(R.id.btnF);
        btnG = findViewById(R.id.btnG);
        btnH = findViewById(R.id.btnH);
        btnI = findViewById(R.id.btnI);
        btnJ = findViewById(R.id.btnJ);
        btnK = findViewById(R.id.btnK);
        btnL = findViewById(R.id.btnL);
        btnM = findViewById(R.id.btnM);
        btnN = findViewById(R.id.btnN);
        btnO = findViewById(R.id.btnO);
        btnP = findViewById(R.id.btnP);
        btnQ = findViewById(R.id.btnQ);
        btnR = findViewById(R.id.btnR);
        btnS = findViewById(R.id.btnS);
        btnT = findViewById(R.id.btnT);
        btnU = findViewById(R.id.btnU);
        btnV = findViewById(R.id.btnV);
        btnW = findViewById(R.id.btnW);
        btnX = findViewById(R.id.btnX);
        btnY = findViewById(R.id.btnY);
        btnZ = findViewById(R.id.btnZ);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onKeyPressed(v);
            }
        };

        btnA.setOnClickListener(onClickListener);
        btnB.setOnClickListener(onClickListener);
        btnC.setOnClickListener(onClickListener);
        btnD.setOnClickListener(onClickListener);
        btnE.setOnClickListener(onClickListener);
        btnF.setOnClickListener(onClickListener);
        btnG.setOnClickListener(onClickListener);
        btnH.setOnClickListener(onClickListener);
        btnI.setOnClickListener(onClickListener);
        btnJ.setOnClickListener(onClickListener);
        btnK.setOnClickListener(onClickListener);
        btnL.setOnClickListener(onClickListener);
        btnM.setOnClickListener(onClickListener);
        btnN.setOnClickListener(onClickListener);
        btnO.setOnClickListener(onClickListener);
        btnP.setOnClickListener(onClickListener);
        btnQ.setOnClickListener(onClickListener);
        btnR.setOnClickListener(onClickListener);
        btnS.setOnClickListener(onClickListener);
        btnT.setOnClickListener(onClickListener);
        btnU.setOnClickListener(onClickListener);
        btnV.setOnClickListener(onClickListener);
        btnW.setOnClickListener(onClickListener);
        btnX.setOnClickListener(onClickListener);
        btnY.setOnClickListener(onClickListener);
        btnZ.setOnClickListener(onClickListener);


        selecetedWord = selectWord();

        dash = dashWord(selecetedWord);
        txtWord.setText(dash);

        imgFace.setImageResource(R.drawable.f1);
    }

    private String selectWord() {
        String[] words = {"developer", "bury", "pour", "title", "blue eyes", "upcoming", "mellow", "foolish"
                , "continue", "juicy", "tie", "direction"};

        int randomeIndex = (int) (Math.random() * words.length);
        return words[randomeIndex];
    }

    private String dashWord(String string) {
        String dash = "";
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {
                dash += "-";
            } else {
                dash += " ";
            }

        }
        return dash;
    }

    private void onKeyPressed(View view) {
        Button button = (Button) view;
        String id = getidString(view);
        String letter = id.replace("btn", "").toLowerCase();
        char charLetter = letter.charAt(0);
        String wordLowercase = selecetedWord.toLowerCase();
        if (wordLowercase.contains(letter)) {
            for (int i = 0; i < wordLowercase.length(); i++) {
                if (wordLowercase.charAt(i) == charLetter) {
                    dash = replacechar(dash, i, selecetedWord.charAt(i));
                    txtWord.setText(dash);
                }

                if (!dash.contains("-")) {
                    winGame();
                    return;
                }

            }


        } else {
            faildcount++;
            sound();

            if (faildcount >= 8) {
                lostgame();
                return;
            }

            imgFace.setImageResource(getImageId(this, "f" + (faildcount + 1)));
        }

        button.setVisibility(View.INVISIBLE);
    }

    private void lostgame() {
        Intent intent = new Intent(MainActivity.this, FinishActivity.class);
        intent.putExtra("RESULT", "LOST");
        startActivity(intent);
        finish();
    }

    private void winGame() {
        Intent intent = new Intent(MainActivity.this, FinishActivity.class);
        intent.putExtra("RESULT", "WON");
        startActivity(intent);
        finish();
    }

    private String replacechar(String dash, int i, char charAt) {
        char[] chars = dash.toCharArray();
        chars[i] = charAt;
        return new String(chars);
    }

    private String getidString(View view) {
        String id = view.getResources().getResourceEntryName(view.getId());
        return id;
    }

    public void sound() {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        mediaPlayer.start();
    }


}
